PiFinder++
==========
PiFinder++ (``pifinder_plusplus``) allows you to find out the IP address of your PI.
All that's needed is to add the simple oneliner below to run on startup and it'll be working.
``curl -X POST -F 'username=yourusername' -F "ip=$(ip a)" http://server_ip:8080/upload``

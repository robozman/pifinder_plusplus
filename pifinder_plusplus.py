#!/usr/bin/env python3

import datetime
from bottle import Bottle, run, get, post, request, response

app = Bottle()

pi_ips = {}

@app.get('/')
def root():
    return 'Hello!'

@app.post('/upload')
def upload():
    username = request.forms.get('username')
    ip = request.forms.get('ip')
    print(ip)
    pi_ips[username] = [ip, str(datetime.datetime.now())]

@app.get('/download/<username>')
def download(username):
    response.content_type = 'text/plain; charset=UTF8' 
    if username in pi_ips:
        return 'Timestamp: ' + pi_ips[username][1] + '\n\n\n\n' + pi_ips[username][0]
    else:
        return 'ERROR: no ip in database'

@app.get('/database')
def database():
    return_string = ""
    for key, value in pi_ips.items():
        return_string += (key +  ':' + '\n')
        return_string += (value[1] + '\n\n')
        return_string += (value[0] + '\n\n\n')
    response.content_type = 'text/plain; charset=UTF8'
    return return_string

run(app, host='localhost', port=8080, reloader=True)

